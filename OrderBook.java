package bcas.assi.bookstore;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class OrderBook extends JFrame {
	public OrderBook() {
		getContentPane().setBackground(Color.PINK);
		getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("OrderBook");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(189, 21, 102, 30);
		getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("User Id");
		lblNewLabel_1.setBounds(46, 78, 46, 14);
		getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("User Name");
		lblNewLabel_2.setBounds(46, 103, 64, 20);
		getContentPane().add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Book Detail");
		lblNewLabel_3.setBounds(46, 134, 64, 14);
		getContentPane().add(lblNewLabel_3);

		textField = new JTextField();
		textField.setBounds(149, 75, 86, 20);
		getContentPane().add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(149, 103, 86, 20);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(149, 134, 86, 20);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("Date");
		lblNewLabel_4.setBounds(277, 78, 46, 14);
		getContentPane().add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("Qty");
		lblNewLabel_5.setBounds(277, 106, 46, 14);
		getContentPane().add(lblNewLabel_5);

		textField_3 = new JTextField();
		textField_3.setBounds(338, 75, 86, 20);
		getContentPane().add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setBounds(338, 103, 86, 20);
		getContentPane().add(textField_4);
		textField_4.setColumns(10);

		JButton btnNewButton = new JButton("conform");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ShoppingCard.main(new String[] {});

			}
		});
		btnNewButton.setBounds(249, 188, 89, 23);
		getContentPane().add(btnNewButton);
	}

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderBook frame = new OrderBook();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
