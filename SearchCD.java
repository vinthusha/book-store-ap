package bcas.assi.bookstore;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import bcas.assi.bookstore.Category;
import bcas.assi.bookstore.View;

public class SearchCD extends JFrame {
	private JTextField textField;
	private JTextField textField_1;

	public SearchCD() {
		getContentPane().setBackground(Color.PINK);
		getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Search CD");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(179, 21, 93, 28);
		getContentPane().add(lblNewLabel);

		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Category.main(new String[] {});
			}
		});
		btnNewButton.setBounds(39, 193, 89, 23);
		getContentPane().add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Search");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				View.main(new String[] {});

			}
		});
		btnNewButton_1.setBounds(269, 193, 89, 23);
		getContentPane().add(btnNewButton_1);

		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBounds(39, 69, 46, 14);
		getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setBounds(39, 119, 46, 14);
		getContentPane().add(lblNewLabel_2);

		textField = new JTextField();
		textField.setBounds(117, 66, 86, 20);
		getContentPane().add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(117, 116, 86, 20);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchCD frame = new SearchCD();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
