package bcas.assi.bookstore;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDbAdd {
	public static int save(String userId, String name, String password, String email, String address, String status) {
		int stat = 0;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"insert into user(userId,name,password,email,address,status) values(?,?,?,?,?,?)");

			ps.setString(1, userId);
			ps.setString(2, name);
			ps.setString(3, password);
			ps.setString(4, email);
			ps.setString(5, address);
			ps.setString(6, status);

			stat = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return stat;
	}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from librarian where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static boolean validate(String name, String password) {
		boolean status = false;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from user where name=? and password=?");
			ps.setString(1, name);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			status = rs.next();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

}
